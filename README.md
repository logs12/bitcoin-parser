## Инициализация окружения ##
1. Необходимо поставить в систему docker. Скачать установочный пакет можно по адресу https://www.docker.com/

2. Необходимо в корне папки проекта создать файл .env из .env.sample со всеми переменными process.env

3. Старт bitcoin-telnet из docker контейнера:

    Pull the image
  * `docker pull freewil/bitcoin-testnet-box`

  or build it yourself from this directory
  * `docker build -t bitcoin-testnet-box .`

    The docker image will run two bitcoin nodes in the background and is meant to be
attached to allow you to type in commands. The image also exposes
the two JSON-RPC ports from the nodes if you want to be able to access them
from outside the container.

   `docker run -t -i --name bitcoind -p 19001:19001 -p 19011:19011 freewil/bitcoin-testnet-box`

    `/Applications/Bitcoin-Qt.app/Contents/MacOS/Bitcoin-Qt -regtest -dnsseed=0 -connect=0.0.0.0:19001 -datadir=./bitcoin-parser/bitcoins/ -splash=0`
