module.exports = {
    //parser: 'babel-eslint',
    extends: [
      'airbnb',
    ],

    globals: {
      "window": true,
      "define": true,
      "require": true,
      "module": true,
    },
};
